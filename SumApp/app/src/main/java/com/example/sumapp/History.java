package com.example.sumapp;

import android.app.DownloadManager;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.webkit.URLUtil;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class History extends AppCompatActivity {

    static final String[] SubTheme1 = new String[] {"1", "2","3"};
    TextView Tv;
    String myHTTP1;
    String Tittle;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);


        ListView listView = (ListView) findViewById(R.id.listView1);
        ArrayAdapter adapter = new ArrayAdapter<String>(getApplicationContext(),
                R.layout.list_item, R.id.txt, SubTheme1);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View view, int position, long arg3) {
                // TODO Auto-generated method stub
                ViewGroup vg = (ViewGroup) view;
                TextView tv = (TextView) vg.findViewById(R.id.txt1);
                Toast.makeText(getApplicationContext(), tv.getText(), Toast.LENGTH_SHORT).show();
                switch (tv.getText().toString()) {
                    case ("1"): {
                        myHTTP1 = "https://drive.google.com/uc?export=download&id=0B04WA-wnBJT-UDZyWktFTURqQmM";
                        Tittle = "מדינת לאום";
                        DM(myHTTP1, Tittle);
                    }
                    case ("2"): {
                        myHTTP1 = "https://drive.google.com/uc?export=download&id=0B04WA-wnBJT-dEZUOVJVaE9PMFk";
                        Tittle = "מאפיינים יהודיים";
                        DM(myHTTP1, Tittle);
                    }
                    case ("3"): {
                        myHTTP1 = "https://drive.google.com/uc?export=download&id=0B04WA-wnBJT-emV3RUdvMkQ2WjA";
                        Tittle = "מדינת ישראל והיהודים בתפוצות";
                        DM(myHTTP1, Tittle);
                    }


                }

            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_history
                , menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void DM (String myHTTP, String Tittle1)
    {
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(myHTTP));
        request.setTitle(Tittle1);
        request.setDescription("History");

        request.allowScanningByMediaScanner();
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE);

        String FileName = URLUtil.guessFileName(myHTTP, null, MimeTypeMap.getFileExtensionFromUrl(myHTTP));
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, FileName);

        DownloadManager manager = (DownloadManager) getApplicationContext().getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);



    }

}
