package com.example.sumapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.sql.Blob;


public class MySqliteHelper extends SQLiteOpenHelper {

    public static final String COLUMN_USER = "user";
    public static final String COLUMN_PASSWORD = "password";
    public static final String COLUMN_EMAIL = "email";
    public static final String COLUMN_NAME = "name";
    public static final Blob COLUMN_IMAGE = null;

   // public static final String COLUMN_GENDER = "gender";

    public static final String[] DB_COL = new String[]{COLUMN_USER, COLUMN_PASSWORD, COLUMN_EMAIL, COLUMN_NAME};

    public static final String DB_NAME = "DB";
    public static final String TABLE_NAME = "dictionaryTableName";

    private static final int DATABASE_VERSION = 3;
    // Database creation sql statement
    private static final String DATABASE_CREATE =
            "CREATE TABLE IF NOT EXISTS " + TABLE_NAME +
                    "(" + COLUMN_USER + " text not null," +
                    COLUMN_PASSWORD + " text not null," + COLUMN_EMAIL + " text not null," + COLUMN_NAME + " text not null)";


    public boolean Insert (String user, String password, String email, String name) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_USER, user);
        contentValues.put(COLUMN_PASSWORD, password);
        contentValues.put(COLUMN_EMAIL, email);
        contentValues.put(COLUMN_NAME, name);
        db.insert("dictionaryTableName", null, contentValues);
        return true;
    }

    public Cursor getData(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from contacts where id="+id+"", null );
        return res;
    }

    public void UpdateAcc(String username,String NewUser,String NewPass,String NewEmail,String NewName,String password)
    {
        SQLiteDatabase db = this.getReadableDatabase();
        db.execSQL("DELETE FROM " + TABLE_NAME+ " WHERE " +COLUMN_USER+ "= '"+username+"'AND "+COLUMN_PASSWORD+"='"+password+"'");
        db.close();

        Insert(NewUser,NewPass,NewEmail,NewName);
    }


    public MySqliteHelper(Context context) {
        super(context, DB_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onCreate(db);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {

        super.onOpen(db);

    }
}
