package com.example.sumapp;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Blob;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Signup extends AppCompatActivity {
    int TAKE_PHOTO_CODE = 0;
    EditText user;
    String user1;
    public static int count = 0;

    private Uri fileUri;
    Button b;
    private ImageView imageHolder;
    private final int requestCode = 20;
    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    public static final int MEDIA_TYPE_IMAGE = 1;

    /*
 * Capturing Camera Image will lauch camera app requrest image capture
 */


    private SQLiteDatabase database1 = null;
    private MySqliteHelper dbHelper = null;
    SharedPreferences sp;
    Uri file;
private String getPictureName() {
   SimpleDateFormat sdf =new SimpleDateFormat("yyyyMMdd_HHmmss");
     String time = sdf.format(new Date());
   return  "PlatPlacesImage" + time + ".jpg";
}
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        sp = getSharedPreferences("FileName",MODE_PRIVATE);
        final SharedPreferences.Editor editor= sp.edit();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        user = (EditText) findViewById(R.id.editText2);
        getSupportActionBar().hide();
        final String picName = getPictureName();
        b = (Button) findViewById(R.id.button14);

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                File PicSave = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

                File imageFile = new File(PicSave,picName);
                Uri PicUri = FileProvider.getUriForFile(getApplicationContext(),"com.your.package.fileProvider",imageFile);
                intent.putExtra(MediaStore.EXTRA_OUTPUT,PicUri);
                startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
                user1= user.getText().toString();

            }
        });
        database1 = this.openOrCreateDatabase(MySqliteHelper.DB_NAME, MODE_PRIVATE, null);
        dbHelper = new MySqliteHelper(getApplicationContext());
        imageHolder = (ImageView) findViewById(R.id.imageView3);
        Button in_sql = (Button) findViewById(R.id.button);
        in_sql.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        EditText name = (EditText) findViewById(R.id.editText);
                        user1 = user.getText().toString();
                        EditText password = (EditText) findViewById(R.id.editText3);
                        EditText email = (EditText) findViewById(R.id.editText4);
                        TextView error1 = (TextView) findViewById(R.id.textView5);
                        RadioButton rb = (RadioButton) findViewById(R.id.radioButton);
                        RadioButton rb2 = (RadioButton) findViewById(R.id.radioButton2);
                        String name1 = name.getText().toString();

                        String password1 = password.getText().toString();
                        String email1 = email.getText().toString();
                        String error = "";
                        String error2 = "";
                        String error3 = "";
                        String error4 = "";
                        String error5 = "";
                        String errorAll = "";

                        String Text = "";
                        editor.putString(user1, picName);
                        editor.apply();
                        if (name1.length() < 2 || user1.length() < 2 || password1.length() < 6 || email1.length() < 5 || !rb.isChecked() || !rb2.isChecked()) {

                            if (name1.length() < 2) {
                                error = error + "First name should be at least 2 char, ";
                            } else {
                                error = "";
                            }
                            if (user1.length() < 2) {
                                error2 = error2 + "Username should be at least 3 char, ";

                            } else {
                                error2 = "";

                            }
                            if (password1.length() < 6) {
                                error3 = error3 + "Password should be at least 6 char, ";
                            } else {
                                error3 = "";
                            }
                            if (!rb.isChecked() && !rb2.isChecked()) {
                                error4 = "Choose your gender, ";
                            } else {
                                error4 = "";
                            }
                            if (email1.length() < 5) {
                                error5 = "Email is too short or invalid, ";
                            } else {
                                error5 = "";
                            }


                            errorAll = error + error2 + error3 + error4 + error5;
                            error1.setText(errorAll);
                            if (rb.isChecked()) {
                                Text = "Female";
                            } else if (rb2.isChecked()) {
                                Text = "male";
                            }
                        }
                        if (errorAll.equals("")) {

                            dbHelper.Insert(user1, password1, email1, name1);
                            Toast.makeText(getBaseContext(), "Insert", Toast.LENGTH_SHORT).show();
                            name.clearAnimation();
                            Intent intent = new Intent(getApplicationContext(), Login.class);
                            startActivity(intent);
                        }
                    }


                });

    }




    protected void onActivityResult(int requestCode, int resultCode, Intent data)  {
        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE)
        {

            if (resultCode == RESULT_OK)
            {
               // imageData = (Bitmap) data.getExtras().get("data");
                //imageHolder.setImageBitmap(imageData);


            }

        }
    }


}

