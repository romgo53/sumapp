package com.example.sumapp;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.Objects;

public class TestYourself extends AppCompatActivity {

    private String readFromFile(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("Question.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                receiveString = bufferedReader.readLine();

                inputStream.close();
                ret = receiveString;
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }
    private String readFromFile1(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("Ans1.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                receiveString = bufferedReader.readLine();
                inputStream.close();
                ret = receiveString;
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }
    private String readFromFile2(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("Ans2.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                receiveString = bufferedReader.readLine();
                inputStream.close();
                ret = receiveString;
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }
    private String readFromFile3(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("Ans3.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                receiveString = bufferedReader.readLine();
                inputStream.close();
                ret = receiveString;
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }
    private String readFromFile4(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("Cans.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                receiveString = bufferedReader.readLine();
                inputStream.close();
                ret = receiveString;
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }
    private String readFromFile5(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("Question2.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                receiveString = bufferedReader.readLine();

                inputStream.close();
                ret = receiveString;
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }
    private String readFromFile6(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("Ans1Q2.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                receiveString = bufferedReader.readLine();
                inputStream.close();
                ret = receiveString;
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }
    private String readFromFile7(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("Ans2Q2.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                receiveString = bufferedReader.readLine();
                inputStream.close();
                ret = receiveString;
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }
    private String readFromFile8(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("Ans3Q2.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                receiveString = bufferedReader.readLine();
                inputStream.close();
                ret = receiveString;
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }
    private String readFromFile9(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("CansQ2.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                receiveString = bufferedReader.readLine();
                inputStream.close();
                ret = receiveString;
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }
    private String readFromFile10(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("Question3.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                receiveString = bufferedReader.readLine();

                inputStream.close();
                ret = receiveString;
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }
    private String readFromFile11(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("Ans1Q3.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                receiveString = bufferedReader.readLine();
                inputStream.close();
                ret = receiveString;
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }
    private String readFromFile12(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("Ans2Q3.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                receiveString = bufferedReader.readLine();
                inputStream.close();
                ret = receiveString;
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }
    private String readFromFile13(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("Ans3Q3.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                receiveString = bufferedReader.readLine();
                inputStream.close();
                ret = receiveString;
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }
    private String readFromFile14(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("CansQ3.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                receiveString = bufferedReader.readLine();
                inputStream.close();
                ret = receiveString;
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }
    private String readFromFile15(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("Question4.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                receiveString = bufferedReader.readLine();

                inputStream.close();
                ret = receiveString;
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }
    private String readFromFile16(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("Ans1Q4.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                receiveString = bufferedReader.readLine();
                inputStream.close();
                ret = receiveString;
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }
    private String readFromFile17(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("Ans2Q4.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                receiveString = bufferedReader.readLine();
                inputStream.close();
                ret = receiveString;
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }
    private String readFromFile18(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("Ans3Q4.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                receiveString = bufferedReader.readLine();
                inputStream.close();
                ret = receiveString;
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }
    private String readFromFile19(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("CansQ4.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                receiveString = bufferedReader.readLine();
                inputStream.close();
                ret = receiveString;
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }
    private String readFromFile20(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("Question5.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                receiveString = bufferedReader.readLine();

                inputStream.close();
                ret = receiveString;
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }
    private String readFromFile21(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("Ans1Q5.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                receiveString = bufferedReader.readLine();
                inputStream.close();
                ret = receiveString;
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }
    private String readFromFile22(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("Ans2Q5.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                receiveString = bufferedReader.readLine();
                inputStream.close();
                ret = receiveString;
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }
    private String readFromFile23(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("Ans3Q5.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                receiveString = bufferedReader.readLine();
                inputStream.close();
                ret = receiveString;
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }
    private String readFromFile24(Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("CansQ5.txt");

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                receiveString = bufferedReader.readLine();
                inputStream.close();
                ret = receiveString;
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }
    TextView Q;
    Button Ans1;
    Button Ans2;
    Button Ans3;
    TextView tv;
    int Counter=0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_yourself);
        getSupportActionBar().hide();
        Counter = 0;
        tv = (TextView) findViewById(R.id.textView9);
        Q = (TextView) findViewById(R.id.textView8);
        Ans1 = (Button) findViewById(R.id.button11);
        Ans2 = (Button) findViewById(R.id.button12);
        Ans3 = (Button) findViewById(R.id.button13);
        String Cans = readFromFile4(getApplicationContext());
        String Cans2 = readFromFile9(getApplicationContext());
        String Cans3 = readFromFile14(getApplicationContext());
        String Cans4 = readFromFile19(getApplicationContext());
        String Cans5 = readFromFile24(getApplicationContext());
        setQ(Q, Ans1, Ans2, Ans3);
        if (Objects.equals(Cans, "1")) {
            Ans1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getApplicationContext(), "Correct",
                            Toast.LENGTH_SHORT).show();
                    Counter++;
                    tv.setText(Counter+"/5");
                }
            });
            Ans2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getApplicationContext(), "Wrong try again",
                            Toast.LENGTH_SHORT).show();
                }
            });

            Ans3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getApplicationContext(), "Wrong try again",
                            Toast.LENGTH_SHORT).show();
                }
            });
        }
        if (Objects.equals(Cans, "2")) {
            Ans2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getApplicationContext(), "Correct",
                            Toast.LENGTH_SHORT).show();

                     Counter++;
                        tv.setText(Counter + "/5");
                }
            });
            Ans1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getApplicationContext(), "Wrong try again",
                            Toast.LENGTH_SHORT).show();
                }
            });

            Ans3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getApplicationContext(), "Wrong try again",
                            Toast.LENGTH_SHORT).show();
                }
            });
        }
        if (Objects.equals(Cans, "3")) {
            Ans3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getApplicationContext(), "Correct",
                            Toast.LENGTH_SHORT).show();
                    Counter++;
                    tv.setText(Counter + "/5");
                }
            });
            Ans2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getApplicationContext(), "Wrong try again",
                            Toast.LENGTH_SHORT).show();
                }
            });

            Ans1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getApplicationContext(), "Wrong try again",
                            Toast.LENGTH_SHORT).show();
                }
            });
        }


        setQ2(Q, Ans1, Ans2, Ans3);
        if (Objects.equals(Cans2, "1")) {
            Ans1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getApplicationContext(), "Correct",
                            Toast.LENGTH_SHORT).show();

                    Counter++;
                    tv.setText(Counter + "/5");
                }
            });
            Ans2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getApplicationContext(), "Wrong try again",
                            Toast.LENGTH_SHORT).show();
                }
            });

            Ans3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getApplicationContext(), "Wrong try again",
                            Toast.LENGTH_SHORT).show();
                }
            });
        }
        if (Objects.equals(Cans2, "2")) {
            Ans2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getApplicationContext(), "Correct",
                            Toast.LENGTH_SHORT).show();
                    Counter++;
                    tv.setText(Counter + "/5");

                }
            });
            Ans1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getApplicationContext(), "Wrong try again",
                            Toast.LENGTH_SHORT).show();
                }
            });

            Ans3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getApplicationContext(), "Wrong try again",
                            Toast.LENGTH_SHORT).show();
                }
            });
        }
        if (Objects.equals(Cans2, "3")) {
            Ans3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getApplicationContext(), "Correct",
                            Toast.LENGTH_SHORT).show();
                    Counter++;
                    tv.setText(Counter + "/5");
                }
            });
            Ans2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getApplicationContext(), "Wrong try again",
                            Toast.LENGTH_SHORT).show();
                }
            });

            Ans1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getApplicationContext(), "Wrong try again",
                            Toast.LENGTH_SHORT).show();
                }
            });

            setQ3(Q, Ans1, Ans2, Ans3);
            if (Objects.equals(Cans3, "1")) {
                Ans1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(getApplicationContext(), "Correct",
                                Toast.LENGTH_SHORT).show();
                        Counter++;
                        tv.setText(Counter+"/5");
                    }
                });
                Ans2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(getApplicationContext(), "Wrong try again",
                                Toast.LENGTH_SHORT).show();
                    }
                });

                Ans3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(getApplicationContext(), "Wrong try again",
                                Toast.LENGTH_SHORT).show();
                    }
                });
            }
            if (Objects.equals(Cans3, "2")) {
                Ans2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(getApplicationContext(), "Correct",
                                Toast.LENGTH_SHORT).show();

                        Counter++;
                        tv.setText(Counter + "/5");
                    }
                });
                Ans1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(getApplicationContext(), "Wrong try again",
                                Toast.LENGTH_SHORT).show();
                    }
                });

                Ans3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(getApplicationContext(), "Wrong try again",
                                Toast.LENGTH_SHORT).show();
                    }
                });
            }
            if (Objects.equals(Cans3, "3")) {
                Ans3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(getApplicationContext(), "Correct",
                                Toast.LENGTH_SHORT).show();
                        Counter++;
                        tv.setText(Counter + "/5");
                    }
                });
                Ans2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(getApplicationContext(), "Wrong try again",
                                Toast.LENGTH_SHORT).show();
                    }
                });

                Ans1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(getApplicationContext(), "Wrong try again",
                                Toast.LENGTH_SHORT).show();
                    }
                });
            }
            setQ4(Q, Ans1, Ans2, Ans3);
            if (Objects.equals(Cans4, "1")) {
                Ans1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(getApplicationContext(), "Correct",
                                Toast.LENGTH_SHORT).show();
                        Counter++;
                        tv.setText(Counter+"/5");
                    }
                });
                Ans2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(getApplicationContext(), "Wrong try again",
                                Toast.LENGTH_SHORT).show();
                    }
                });

                Ans3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(getApplicationContext(), "Wrong try again",
                                Toast.LENGTH_SHORT).show();
                    }
                });
            }
            if (Objects.equals(Cans4, "2")) {
                Ans2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(getApplicationContext(), "Correct",
                                Toast.LENGTH_SHORT).show();

                        Counter++;
                        tv.setText(Counter + "/5");
                    }
                });
                Ans1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(getApplicationContext(), "Wrong try again",
                                Toast.LENGTH_SHORT).show();
                    }
                });

                Ans3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(getApplicationContext(), "Wrong try again",
                                Toast.LENGTH_SHORT).show();
                    }
                });
            }
            if (Objects.equals(Cans4, "3")) {
                Ans3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(getApplicationContext(), "Correct",
                                Toast.LENGTH_SHORT).show();
                        Counter++;
                        tv.setText(Counter + "/5");
                    }
                });
                Ans2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(getApplicationContext(), "Wrong try again",
                                Toast.LENGTH_SHORT).show();
                    }
                });

                Ans1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(getApplicationContext(), "Wrong try again",
                                Toast.LENGTH_SHORT).show();
                    }
                });
            }
            setQ5(Q, Ans1, Ans2, Ans3);
            if (Objects.equals(Cans5, "1")) {
                Ans1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(getApplicationContext(), "Correct",
                                Toast.LENGTH_SHORT).show();
                        Counter++;
                        tv.setText(Counter+"/5");
                    }
                });
                Ans2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(getApplicationContext(), "Wrong try again",
                                Toast.LENGTH_SHORT).show();
                    }
                });

                Ans3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(getApplicationContext(), "Wrong try again",
                                Toast.LENGTH_SHORT).show();
                    }
                });
            }
            if (Objects.equals(Cans5, "2")) {
                Ans2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(getApplicationContext(), "Correct",
                                Toast.LENGTH_SHORT).show();

                        Counter++;
                        tv.setText(Counter + "/5");
                    }
                });
                Ans1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(getApplicationContext(), "Wrong try again",
                                Toast.LENGTH_SHORT).show();
                    }
                });

                Ans3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(getApplicationContext(), "Wrong try again",
                                Toast.LENGTH_SHORT).show();
                    }
                });
            }
            if (Objects.equals(Cans5, "3")) {
                Ans3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(getApplicationContext(), "Correct",
                                Toast.LENGTH_SHORT).show();
                        Counter++;
                        tv.setText(Counter + "/5");
                    }
                });
                Ans2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(getApplicationContext(), "Wrong try again",
                                Toast.LENGTH_SHORT).show();
                    }
                });

                Ans1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(getApplicationContext(), "Wrong try again",
                                Toast.LENGTH_SHORT).show();
                    }
                });
            }
                if (Counter <= 5) {
                Toast.makeText(this, "Well done!", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(getApplicationContext(), citizen.class);
                startActivity(i);
            }

        }
    }

    void setQ(TextView Q,Button Ans1,Button Ans2,Button Ans3)
    {
        Q.setText(readFromFile(getApplicationContext()));
        Ans1.setText(readFromFile1(getApplicationContext()));
        Ans2.setText(readFromFile2(getApplicationContext()));
        Ans3.setText(readFromFile3(getApplicationContext()));

    }
    void setQ2(TextView Q,Button Ans1,Button Ans2,Button Ans3)
    {
        Q.setText(readFromFile5(getApplicationContext()));
        Ans1.setText(readFromFile6(getApplicationContext()));
        Ans2.setText(readFromFile7(getApplicationContext()));
        Ans3.setText(readFromFile8(getApplicationContext()));

    }
    void setQ3(TextView Q,Button Ans1,Button Ans2,Button Ans3)
    {
        Q.setText(readFromFile10(getApplicationContext()));
        Ans1.setText(readFromFile11(getApplicationContext()));
        Ans2.setText(readFromFile12(getApplicationContext()));
        Ans3.setText(readFromFile13(getApplicationContext()));

    }
    void setQ4(TextView Q,Button Ans1,Button Ans2,Button Ans3)
    {
        Q.setText(readFromFile15(getApplicationContext()));
        Ans1.setText(readFromFile16(getApplicationContext()));
        Ans2.setText(readFromFile17(getApplicationContext()));
        Ans3.setText(readFromFile18(getApplicationContext()));

    }
    void setQ5(TextView Q,Button Ans1,Button Ans2,Button Ans3)
    {
        Q.setText(readFromFile20(getApplicationContext()));
        Ans1.setText(readFromFile21(getApplicationContext()));
        Ans2.setText(readFromFile22(getApplicationContext()));
        Ans3.setText(readFromFile23(getApplicationContext()));

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_test_yourself, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
