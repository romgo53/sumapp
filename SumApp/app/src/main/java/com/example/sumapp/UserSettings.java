package com.example.sumapp;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;

public class UserSettings extends AppCompatActivity {
    SharedPreferences sp;
    ImageView iv;
    TextView Name;
    Button Cp;
    Button Cu;
    Button Ce;
    private SQLiteDatabase database = null;
    private MySqliteHelper dbHelper = null;


SharedPreferences sp1;
SharedPreferences sp2;
            @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_settings);
        getSupportActionBar().hide();
         sp = getSharedPreferences("LogInUser", MODE_PRIVATE);
        iv=(ImageView) findViewById(R.id.imageView2);
        sp2=getSharedPreferences("FileName",MODE_PRIVATE);
        String pic = sp2.getString(sp.getString("User",""),"");
        File imgFile = new  File("/storage/emulated/0/Pictures/"+pic);
        Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
        iv.setImageBitmap(myBitmap);


        Name = (TextView) findViewById(R.id.textView7);
        Name.setText("Hello, " + sp.getString("Name", "User"));
        Cp = (Button) findViewById(R.id.button5);
        sp1= getSharedPreferences("sp",MODE_PRIVATE);
        SharedPreferences.Editor edit= sp1.edit();
                //Bitmap bitmap  = getIntent().getExtras().getParcelable("name");
                //iv.setImageBitmap(bitmap);
                Cu = (Button) findViewById(R.id.button7);
        Ce = (Button) findViewById(R.id.button6);
        dbHelper = new MySqliteHelper(getApplicationContext());
        database = this.openOrCreateDatabase(MySqliteHelper.DB_NAME, MODE_PRIVATE, null);
        Cp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alert = new AlertDialog.Builder(UserSettings.this);
                final EditText editText = new EditText(getApplicationContext());
                alert.setTitle("Change password");
                alert.setMessage("Enter new password:");
                alert.setView(editText);
                alert.setPositiveButton("Change", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String newPass = editText.getText().toString();
                        dbHelper.UpdateAcc(sp.getString("User",""),sp.getString("User",""),newPass,sp.getString("Email",""),sp.getString("Name",""),sp.getString("Password",""));
                        Toast.makeText(getApplicationContext(),"Password changed",Toast.LENGTH_SHORT).show();
                        Intent intent2=new Intent(getApplicationContext(),MainActivity.class);

                    }
                });
                AlertDialog alertDialog = alert.create();
                alertDialog.show();
            }
        });


        Ce.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alert2 = new AlertDialog.Builder(UserSettings.this);
                final EditText editText = new EditText(getApplicationContext());
                alert2.setTitle("Change Email");
                alert2.setMessage("Enter new email:");
                alert2.setView(editText);
                alert2.setPositiveButton("Change", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String newEmail = editText.getText().toString();
                        dbHelper.UpdateAcc(sp.getString("User",""),sp.getString("User",""),sp.getString("Password",""),newEmail,sp.getString("Name",""),sp.getString("Password",""));
                        Toast.makeText(getApplicationContext(),"Email changed",Toast.LENGTH_SHORT).show();
                        Intent intent1=new Intent(getApplicationContext(),MainActivity.class);

                    }
                });
                AlertDialog alertDialog2 = alert2.create();
                alertDialog2.show();
            }
        });


        Cu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alert1 = new AlertDialog.Builder(UserSettings.this);
                final EditText editText = new EditText(getApplicationContext());
                alert1.setTitle("Change UserName");
                alert1.setMessage("Enter new username:");
                alert1.setView(editText);
                alert1.setPositiveButton("Change", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String newUser = editText.getText().toString();
                        dbHelper.UpdateAcc(newUser, sp.getString("User", ""), sp.getString("Password", ""), sp.getString("Email", ""), sp.getString("Name", ""), sp.getString("Password", ""));
                        Toast.makeText(getApplicationContext(),"UserName changed",Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(getApplicationContext(),MainActivity.class);
                    }
                });
                AlertDialog alertDialog1 = alert1.create();
                alertDialog1.show();
            }
        });

    }


}
