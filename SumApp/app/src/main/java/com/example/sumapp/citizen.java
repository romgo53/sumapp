package com.example.sumapp;

import android.app.Dialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.webkit.URLUtil;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.sql.DatabaseMetaData;
import java.text.SimpleDateFormat;
import java.util.Date;

public class citizen extends AppCompatActivity {

    private String getFileName() {
        SimpleDateFormat sdf =new SimpleDateFormat("yyyyMMdd_HHmmss");
        String time = sdf.format(new Date());
        return  "Summery" + time;
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case progress_bar_type:
                pDialog = new ProgressDialog(this);
                pDialog.setMessage("Downloading file. Please wait...");
                pDialog.setIndeterminate(false);
                pDialog.setMax(100);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setCancelable(true);
                pDialog.show();
                return pDialog;
            default:
                return null;
        }
    }
    static final String[] SubTheme = new String[] { "מדינת לאום", "מאפיינים יהודיים","מדינת ישראל והיהודים בתפוצות","הצע תת-נושא"};
    static final String[] SubTxt = new String[] { "מדינת לאום", "מאפיינים יהודיים","מדינת ישראל והיהודים בתפוצות"};

    TextView Tv;
    String mimeT="";
    String myHTTP1;
    String Tittle;
    Button b1;
    String FileName = getFileName();
    private ProgressDialog pDialog;
    public static final int progress_bar_type = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_citizen);
        getSupportActionBar().hide();
        b1= (Button) findViewById(R.id.button8);
        ListView listView = (ListView) findViewById(R.id.listView);
        ArrayAdapter adapter = new ArrayAdapter<String>(getApplicationContext(),
                R.layout.list_item, R.id.txt, SubTheme);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View view, int position, long arg3) {
                // TODO Auto-generated method stub
                ViewGroup vg = (ViewGroup) view;

                TextView tv = (TextView) vg.findViewById(R.id.txt);
                Toast.makeText(getApplicationContext(),tv.getText().toString(), Toast.LENGTH_SHORT).show();
                switch (tv.getText().toString()) {
                    case ("מדינת לאום"): {
                        myHTTP1 = "https://drive.google.com/uc?export=download&id=0B04WA-wnBJT-UDZyWktFTURqQmM";
                        Tittle = "מדינת לאום";
                        mimeT=".doc";
                        new DownloadFileFromURL().execute(myHTTP1);
                    }
                    break;
                    case ("מאפיינים יהודיים"): {
                        myHTTP1 = "https://drive.google.com/uc?export=download&id=0B04WA-wnBJT-dEZUOVJVaE9PMFk";
                        Tittle = "מאפיינים יהודיים";
                        mimeT=".pptx";
                        new DownloadFileFromURL().execute(myHTTP1);                    }
                    break;
                    case ("מדינת ישראל והיהודים בתפוצות"): {
                        myHTTP1 = "https://drive.google.com/uc?export=download&id=0B04WA-wnBJT-emV3RUdvMkQ2WjA";
                        Tittle = "מדינת ישראל והיהודים בתפוצות";
                        mimeT=".doc";
                        new DownloadFileFromURL().execute(myHTTP1);
                    }
                    break;
                    case ("הצע תת-נושא"): {
                       Intent intent=new Intent(getApplicationContext(),Email.class);
                        startActivity(intent);
                    }
                    break;

                }

            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_citizen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void DM (String myHTTP, String Tittle1)
    {

        DownloadManager manager = (DownloadManager) getApplicationContext().getSystemService(Context.DOWNLOAD_SERVICE);
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(myHTTP));
        request.setTitle(Tittle1);
        request.setDescription("Click here to open");
        DownloadManager.Query q=new DownloadManager.Query();


        request.allowScanningByMediaScanner();
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

        String FileName = URLUtil.guessFileName(myHTTP, null, MimeTypeMap.getFileExtensionFromUrl(myHTTP));
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, FileName);


        manager.enqueue(request);


    }

    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(progress_bar_type);
        }

        /**
         * Downloading file in background thread
         * */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // getting file length
                int lenghtOfFile = conection.getContentLength();

                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                // Output stream to write file
                OutputStream output = new FileOutputStream("/sdcard/"+FileName+mimeT);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress(""+(int)((total*100)/lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }


        protected void onProgressUpdate(String... progress) {

            pDialog.setProgress(Integer.parseInt(progress[0]));
        }


        @Override
        protected void onPostExecute(String file_url) {

            dismissDialog(progress_bar_type);
            Intent intent = new Intent();
            intent.setAction(android.content.Intent.ACTION_VIEW);
            File file = new File("/sdcard/"+FileName+mimeT);
            intent.setDataAndType(Uri.fromFile(file), "*/*");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            startActivity(Intent.createChooser(intent, "Open With````````````````````````````````````````````````````````````````````````````````````````````````````````````````````:"));


        }

    }
    public void Test(View view) {
        Intent intent = new Intent(getApplicationContext(),TestYourself.class);
        startActivity(intent);
    }
}
